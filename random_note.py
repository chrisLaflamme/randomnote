'''
TODO: Include module DOCSTRING?
'''
import random
import time

# NOTES
ALL_NOTES = []
FLATS = [
    u'A\u266D',
    u'B\u266D',
    u'D\u266D',
    u'E\u266D',
    u'G\u266D'
]

SHARPS = [
    u'A\u266F',
    u'C\u266F',
    u'D\u266F',
    u'F\u266F',
    u'G\u266F'
]

NATURALS = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G'
]

for flat in FLATS:
    ALL_NOTES.append(flat)
for sharp in SHARPS:
    ALL_NOTES.append(sharp)
for natural in NATURALS:
    ALL_NOTES.append(natural)

# Time Management
SECONDS_TO_RUN = 60
SECONDS_TO_SLEEP = 2


def start_game(run_time, sleep_time, notes_to_display):
    '''
    TODO: Improve DOCSTRING
    Run the game
    '''
    running_time = time.time() + run_time
    sleeping_time = sleep_time
    notes = notes_to_display

    while time.time() < running_time:
        rand = random.randint(0, (len(notes) -1))
        print(f'Randint: {rand}')
        print(f'{len(notes)} found. Choosing {notes[rand]}')
        print(notes[rand])
        time.sleep(sleeping_time)


def main():
    '''
    TODO: Improve DOCSTRING
    This is main()...so...
    '''
    start_game(SECONDS_TO_RUN, SECONDS_TO_SLEEP, ALL_NOTES)


main()
